//
//  ContentView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 08/01/2021.
//

import SwiftUI

struct LoginView: View {
    @StateObject var usersManager = UsersManager()
    var body: some View {
        VStack {
            TextField("Email", text: $usersManager.userEmail)
                .keyboardType(.emailAddress)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            SecureField("Password", text: $usersManager.userPassword)
                .padding()
                .textFieldStyle(RoundedBorderTextFieldStyle())
            Button(action: { usersManager.loginUser() }, label: {
                RoundedRectangle(cornerRadius: 16)
                    .frame(width: 75, height: 45, alignment: .center)
                    .foregroundColor(.white)
                    .overlay(Text("Login"))
                    .padding()
                
            })
            NavigationLink(
                destination: TodosListView(usersManager: usersManager),
                isActive: $usersManager.userLoggedIn,
                label: {
                    EmptyView()
                })
            if usersManager.isLoading {
                ProgressView("Login en cours")
            } else {
                Text(usersManager.logInResult)
            }
            Spacer()
        }
        .navigationBarItems(trailing:
            NavigationLink(
                destination: CreateUserView(),
                label: {
                    HStack {
                        Text("Create user")
                        Image(systemName: "person.crop.circle.fill.badge.plus").font(.system(size: 30))
                    }
            })
        )
        .navigationTitle("Log In")
        .background(Color("AppGrey"))
        .edgesIgnoringSafeArea(.bottom)
        .onAppear {
            if usersManager.userToken != nil{
                print("user log in")
                usersManager.userLoggedIn = true
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            LoginView()
        }
    }
}
