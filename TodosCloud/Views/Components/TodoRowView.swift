//
//  TodoRowView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 12/01/2021.
//

import SwiftUI

struct TodoRowView: View {
    @State var todo: Todo
    @State var isEditing: Bool = false
    @StateObject var todosManager: TodosManager
    var body: some View {
        HStack {
            Image(systemName: "checkmark.circle.fill")
                .resizable()
                .frame(width: 30, height: 30)
            if isEditing {
                TextField(todo.title, text: $todo.title)
            } else {
                Text(todo.title)
            }
            Spacer()
            Button(action: {
                if isEditing {
                    todosManager.updateTodo(todo: todo)
                }
                    isEditing.toggle() },
                   label: {
                Image(systemName: "pencil")
                    .font(.system(size: 30))
            })
            .buttonStyle(PlainButtonStyle())
        }
    }
}

struct TodoRowView_Previews: PreviewProvider {
    static var previews: some View {
        TodoRowView(todo: Todo(id: UUID(), title: "Test"), todosManager: TodosManager())
            .previewLayout(.sizeThatFits)
    }
}
