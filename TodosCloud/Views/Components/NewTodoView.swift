//
//  NewTodoView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 13/01/2021.
//

import SwiftUI

struct NewTodoView: View {
    @StateObject var todosManager: TodosManager
    @State var newTodo: String = ""
    var body: some View {
        HStack {
            TextField("New todo", text: $newTodo)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                
            Button(action: { todosManager.createTodo(withTitle: newTodo) }, label: {
                Image(systemName: "plus.circle")
                    .font(.system(size: 40))
            })
        }
        .padding()
    }
}

struct NewTodoView_Previews: PreviewProvider {
    static var previews: some View {
        NewTodoView(todosManager: TodosManager())
            .previewLayout(.sizeThatFits)
    }
}
