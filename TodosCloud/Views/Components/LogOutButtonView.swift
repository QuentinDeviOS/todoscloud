//
//  LogOutButtonView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 14/01/2021.
//

import SwiftUI

struct LogOutButtonView: View {
    @StateObject var todosManager: TodosManager
    @StateObject var usersManager: UsersManager
    var body: some View {
        Button(action: {
            usersManager.logOut()
        }, label: {
            NavigationLink(
                destination: LoginView(),
                isActive: $usersManager.userLoggedOut,
                label: {
                    Text("Log Out")
                })
        })
    }
}

struct LogOutButtonView_Previews: PreviewProvider {
    static var previews: some View {
        LogOutButtonView(todosManager: TodosManager(), usersManager: UsersManager())
    }
}
