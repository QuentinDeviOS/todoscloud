//
//  CreateUserView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 13/01/2021.
//

import SwiftUI

struct CreateUserView: View {
    @StateObject var usersManager = UsersManager()
    var body: some View {
        VStack {
            TextField("Name", text: $usersManager.userName)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            TextField("Email", text: $usersManager.userEmail)
                .keyboardType(.emailAddress)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            SecureField("Password", text: $usersManager.userPassword)
                .padding()
                .textFieldStyle(RoundedBorderTextFieldStyle())
            Button(action: { usersManager.createUser() }, label: {
                RoundedRectangle(cornerRadius: 16)
                    .frame(width: 95, height: 45, alignment: .center)
                    .foregroundColor(.white)
                    .overlay(Text("Create user"))
                    .padding()
                
            })
            if usersManager.isLoading {
                ProgressView("User creation in progress")
            }
            Text(usersManager.creationResult)
            Spacer()
        }
        .navigationTitle("Create user")
        .background(Color("AppGrey"))
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct CreateUserView_Previews: PreviewProvider {
    static var previews: some View {
        CreateUserView()
    }
}
