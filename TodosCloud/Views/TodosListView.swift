//
//  TodosListView.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 10/01/2021.
//

import SwiftUI

struct TodosListView: View {
    @StateObject var todosManager = TodosManager()
    @StateObject var usersManager : UsersManager
    var body: some View {
        VStack {
            if todosManager.isLoading {
                ProgressView("Chargement en cours")
            } else {
                if let downloadedTodosList = todosManager.todosList {
                    VStack {
                        NewTodoView(todosManager: todosManager)
                        List {
                            ForEach(downloadedTodosList) { todo in
                                TodoRowView(todo: todo, todosManager: todosManager)
                            }
                            .onDelete(perform: todosManager.deleteTodo) 
                        }
                    } 
                }
            }
            
        }
        .navigationBarItems(trailing: EditButton())

        .navigationTitle("Todos✅")
        .navigationBarBackButtonHidden(true)
        .onAppear {
            todosManager.loadTodosList()
        }
    }
    

    
    
}

struct TodosListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            TodosListView(usersManager: UsersManager())
        }
    }
}
