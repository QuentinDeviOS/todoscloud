//
//  TodosCloudApp.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 08/01/2021.
//

import SwiftUI

@main
struct TodosCloudApp: App {
    var body: some Scene {
        WindowGroup {
            NavigationView {
                LoginView()
            }
        }
    }
}
