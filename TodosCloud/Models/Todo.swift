//
//  Todo.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 11/01/2021.
//

import Foundation

struct Todo: Codable, Identifiable {
    let id: UUID
    var title: String
}
