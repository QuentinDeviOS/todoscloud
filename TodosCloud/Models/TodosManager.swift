//
//  TodosManager.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 12/01/2021.
//

import Foundation
import SwiftUI

class TodosManager: ObservableObject {
    @Published var isLoading: Bool = false
    @Published var todosList: [Todo]?
    @Published var usersManager = UsersManager()
    
    
    func createTodo(withTitle title: String) {
        guard let addTodoAPIUrl = URL(string: "http://51.159.189.83/todos") else { return }
        print(addTodoAPIUrl)
        let urlSession = URLSession.shared
        var request = URLRequest(url: addTodoAPIUrl)
        
        let newTodo = TodoJson(title: title)
        let encoder = JSONEncoder()
        guard let jsonData = try? encoder.encode(newTodo) else { return }
        request.httpBody = jsonData
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        print(String(data: jsonData, encoding: .utf8)!)
        request.httpMethod = "POST"
        if let userTokenValue = usersManager.userToken {
            request.setValue("Bearer \(userTokenValue)", forHTTPHeaderField: "Authorization")
        }
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse
                   else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    print("Todo creation failed")
                }
                return
            }
            if httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 {
            DispatchQueue.main.async {
                self.isLoading = false
                self.loadTodosList()
            }
            print("Success - Todo : \(newTodo.title) created")
            } else {
                print(httpResponse.statusCode)
            }
        }
        dataTask.resume()
    }
    
    func updateTodo(todo: Todo) {
        guard let updateTodoAPIUrl = URL(string: "http://51.159.189.83/todos/\(todo.id)") else { return }
        let urlSession = URLSession.shared
        var request = URLRequest(url: updateTodoAPIUrl)
        
        let encoder = JSONEncoder()
        guard let jsonData = try? encoder.encode(todo) else { return }
        request.httpBody = jsonData
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.httpMethod = "PATCH"
        if let userTokenValue = usersManager.userToken {
            request.setValue("Bearer \(userTokenValue)", forHTTPHeaderField: "Authorization")
        }
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse
                   else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    print("Todo update failed")
                }
                return
            }
            if httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 {
            DispatchQueue.main.async {
                self.isLoading = false
                self.loadTodosList()
            }
            print("Success - Todo : \(todo.title) updated")
            } else {
                print(httpResponse.statusCode)
            }
        }
        dataTask.resume()
    }
    
    func loadTodosList() {
        guard let todosListAPIUrl = URL(string: "http://51.159.189.83/todos") else { return }
        let urlSession = URLSession.shared
        
        var request = URLRequest(url: todosListAPIUrl)
        
        request.httpMethod = "GET"
        if let userTokenValue = usersManager.userToken {
            request.setValue("Bearer \(userTokenValue)", forHTTPHeaderField: "Authorization")
        }
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode >= 200 && httpResponse.statusCode < 300,
                  let todosListJsonData = data
            else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    print("Download failed")
                }
                return
            }
            print("Connection Ok")
            if let downloadedTodosList = try? JSONDecoder().decode([Todo].self, from: todosListJsonData) {
                
                DispatchQueue.main.async {
                    self.todosList = downloadedTodosList
                    self.isLoading = false
                }
            } else {
                print(todosListJsonData.debugDescription)
            }
        }
        dataTask.resume()
    }
    
    func deleteTodo(at offsets: IndexSet) {
        // preserve all ids to be deleted to avoid indices confusing
        guard let todosList = self.todosList else { return }
        let todosIdsToDelete = offsets.map { todosList[$0].id }
        // schedule remote delete for selected ids
            _ = todosIdsToDelete.compactMap { id in
                self.deleteTodoInCloud(id: id)
                print(id)
                }
            
    }
    
    func deleteTodoInCloud(id: UUID) {
        
        guard let deleteTodoAPIUrl = URL(string: "http://51.159.189.83/todos/\(id)") else { return }
        print(deleteTodoAPIUrl)
        let urlSession = URLSession.shared
        var request = URLRequest(url: deleteTodoAPIUrl)
        
        request.httpMethod = "DELETE"
        if let userTokenValue = usersManager.userToken {
            request.setValue("Bearer \(userTokenValue)", forHTTPHeaderField: "Authorization")
        }
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode >= 200 && httpResponse.statusCode < 300 else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    print("Todo deleting failed")
                }
                return
            }
            DispatchQueue.main.async {
                self.isLoading = false
                self.loadTodosList()
            }
            print("Success - Todo : \(id) deleted")
        }
        dataTask.resume()
    }
}
