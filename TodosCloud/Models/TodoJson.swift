//
//  TodoJson.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 13/01/2021.
//

import Foundation

struct TodoJson : Codable {
    let title: String
}
