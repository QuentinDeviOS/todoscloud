//
//  User.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 09/01/2021.
//

import Foundation

struct User: Identifiable, Codable {
    let id: UUID
}
