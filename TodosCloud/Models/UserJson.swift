//
//  UserJson.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 14/01/2021.
//

import Foundation

struct UserJson: Codable {
    let name: String
    let email: String
    let password: String
}
