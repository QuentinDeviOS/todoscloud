//
//  UserToken.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 08/01/2021.
//

import Foundation

struct UserToken: Identifiable, Codable {
    let id: UUID
    let value: String
    let user: User
}
