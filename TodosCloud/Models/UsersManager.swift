//
//  UsersManager.swift
//  TodosCloud
//
//  Created by Quentin ROLLAND on 14/01/2021.
//

import Foundation
import SwiftUI

class UsersManager: ObservableObject {
    @AppStorage("user_token") var userToken: String?
    @Published var isLoading = false
    @Published var userName: String = ""
    @Published var userEmail: String = ""
    @Published var userPassword: String = ""
    @Published var userLoggedIn: Bool = false
    @Published var userLoggedOut: Bool = false
    @Published var creationResult: String = ""
    @Published var logInResult: String = ""
    
    func createUser() {
        guard let createUserAPIUrl = URL(string: "http://51.159.189.83/users") else { return }
        let urlSession = URLSession.shared
        
        var request = URLRequest(url: createUserAPIUrl)
        
        let newUser = UserJson(name: userName, email: userEmail, password: userPassword)
        let encoder = JSONEncoder()
        guard let jsonData = try? encoder.encode(newUser) else { return }
        request.httpBody = jsonData
        //HTTP Headers
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        print(String(data: jsonData, encoding: .utf8)!)
        request.httpMethod = "POST"
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode >= 200 && httpResponse.statusCode < 300
            else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.creationResult = "User creation failed"
                }
                return
            }
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.creationResult = "User created, now you can log in with your email and password"
                }
            }
        
        dataTask.resume()
    }
    
    func loginUser() {
        guard let loginUserAPIUrl = URL(string: "http://51.159.189.83/login") else { return }
        let urlSession = URLSession.shared
        
        var request = URLRequest(url: loginUserAPIUrl)
        
        let loginString = String(format: "%@:%@", userEmail, userPassword)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        request.httpMethod = "POST"
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        isLoading = true
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                  let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode >= 200 && httpResponse.statusCode < 300,
                  let userTokenJsonData = data
            else {
                DispatchQueue.main.async {
                    self.isLoading = false
                    self.logInResult = "Login Failed"
                }
                return
            }
            if let downloadedUsertoken = try? JSONDecoder().decode(UserToken.self, from: userTokenJsonData) {
                
                DispatchQueue.main.async {
                    self.userToken = downloadedUsertoken.value
                    self.isLoading = false
                    self.userLoggedIn = true
                    self.userLoggedOut = false
                    self.logInResult = "Login successfull"
                }
            } else {
                print(userTokenJsonData.debugDescription)
            }
        }
        dataTask.resume()
    }
    
    func logOut() {
        userToken = nil
        userLoggedIn = false
        userLoggedOut = true
    }
    
}
